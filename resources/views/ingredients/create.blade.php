@extends('layouts.app')

@section('title', 'Pizzería con Laravel')

@section('content')
    <h1>Alta de ingredientes</h1>

    <form method="post" action="/ingredients/create">
         {{ csrf_field() }}
         <div>
         <label>Nombre</label>
         <input type="text" name="name" value="{{ old('name') }}">
        {{ $errors->first('name') }}
        </div>

        <div>
         <label>Tipo</label>
         <input type="text" name="type_id" value="{{ old('type_id') }}">
         {{ $errors->first('type_id') }}
         </div>

        <label></label>
        <input type="submit" value="Enviar"><br>
    </form>

    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
@stop