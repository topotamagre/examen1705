
@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de pizzas
</h1>

<a href="/ingredients/create">
Alta de ingredientes
</a>

<table class="table">   

    <tr>
        <th>Id</th>
        <th>Nombre</th>
    </tr>


@foreach ($pizzas as $pizza)
    <tr>
        <td>{{ $pizza->id }}</td>
        <td>{{ $pizza->name }}</td>

        <td>
        <a href="/pizzas/{{ $pizza->id }}">Ver</a>
        <a href="/pizzas/{{ $pizza->id }}/edit">Actualizar</a>

        <form action="/pizzas/{{ $pizza->id }}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        <input type="submit" value="borrar">
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $pizzas->links() }}
</div>
@endsection
