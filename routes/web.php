<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/pizzas', 'PizzaController');
Route::resource('/ingredients', 'IngredientsController');
Route::resource('pizzas/{id}', 'PizzaController@show');
Route::get('pizzas', 'PizzaController@index');
Route::get('ingredients/create', 'IngredientController@create');
Route::get('ingredients/create', 'IngredientController@store');
Route::get('pizzas/delete/{id}', 'PizzaController@delete');
Route::get('ingredients/delete/{id}', 'IngredientController@delete');

//SOLO PARA LOS QUE ESTAN DADOS DE ALTA

Route::get('/', function () {
    return "Este contenido es sólo para usuarios";
})->middleware('auth');
