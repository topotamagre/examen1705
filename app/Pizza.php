<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    public function users()
    {
        return $this->belongTo('App\User');
    }
    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient');
    }
}
