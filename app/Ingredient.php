<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public function users()
    {
        return $this->belongTo('App\User');
    }
    public function types()
    {
        return $this->hasMany('App\Type');
    }

        public function pizzas()
    {
        return $this->belongsToMany('App\Pizza')->withPivot('ingredients');
    }
}
