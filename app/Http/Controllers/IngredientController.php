<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Ingredient;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('auth')->only('index');
        $this->middleware('auth');
    }
    
    public function index()
    {
        $ingredients = Ingredient::paginate(5);
        return view('ingredient.index', ['ingredients' => $ingredients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //con autorize: si no puede-->error 403
        $this->authorize('create', Ingredient::class);

        return view('ingredients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //con autorize: si no puede-->error 403
        $this->authorize('create', Ingredient::class);
        $this->validate($request, [
            'name' => 'required|max:255|min:2',
            'type_id' => 'required|max:255'
        ]);



        $ingredient = new Ingredient($request->all());
        $ingredient->save();
        $id = $ingredient->id;
        return redirect('/ingredients/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ingredient::destroy($id);

        return redirect('/ingredients');

    }
    
}
